import React from 'react';
//https://github.com/asvetliakov/jest-mock-action-creators
//https://www.npmjs.com/package/jest-mock-action-creators
import Adapter from 'enzyme-adapter-react-15';

import { shallow, render, mount, configure } from 'enzyme';

configure({ adapter: new Adapter() });
const intl = {
  login: {
    loggedIn: true,
    rememberMe: {
      email: '',
      password: ''
    }
  }
};

global.intl = intl;

global.shallow = shallow;

global.render = render;

global.mount = mount;
