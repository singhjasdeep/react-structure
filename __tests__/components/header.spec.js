import React from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { Link } from 'react-router-dom';
import Header from '../../src/components/Header';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Header Component', () => {
  it('should render header component without throwing an error', () => {
    const store = mockStore({});
    // eslint-disable-next-line
    const component = shallow(
      <Provider store={store}>
        <Header />
      </Provider>
    );
    expect(component).toMatchSnapshot();
    component.unmount();
  });

  it('should test toggle function in header without throwing an error', () => {
    const mockToggle = jest.fn();
    // eslint-disable-next-line
    const component = shallow(<button onClick={mockToggle} />);
    component.find('button').simulate('click');
    expect(mockToggle.mock.calls.length).toEqual(1);
  });

  it('should test route to page in header without throwing an error', () => {
    // eslint-disable-next-line
    const component = shallow(
      <Router history={createHistory()}>
        <Link to="/about">Order Request</Link>
      </Router>
    );
    expect(component.find(Link).props().to).toEqual('/about');
  });

  it('should test logout function in header without throwing an error', () => {
    const mockLogout = jest.fn();
    // eslint-disable-next-line
    const component = shallow(
      <Router history={createHistory()}>
        <Link to="#" onClick={mockLogout}>
          Log Out
        </Link>
      </Router>
    );
    component.find('Link').simulate('click');
    expect(mockLogout.mock.calls.length).toEqual(1);
  });
});
