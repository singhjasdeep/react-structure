import user, { initialState } from '../../src/reducer/modules/user';
import * as USER from '../../src/actions/user';

describe('user reducer', () => {
  test('login success', () => {
    const data = {
      nickname: 'jasdeepsingh51',
      name: 'jasdeep.singh@smartdatainc.net',
      picture:
        'https://s.gravatar.com/avatar/a829bcb80d2d367be29892f02f32f99e?s=480&r=pg&d=https%3A%2F%2Fcdn.auth0.com%2Favatars%2Fja.png',
      updated_at: '2018-08-28T05:11:01.078Z',
      email: 'jasdeep.singh@smartdatainc.net',
      email_verified: true,
      user_id: 12,
      tenant_id: 2
    };
    const loginAction = USER.login_Success(data);
    expect(user(initialState, loginAction)).toEqual({ ...data, ...initialState, loggedIn: true });
  });

  test('remenber me', () => {
    const data = { email: 'jasdeep.singh@smartdatainc.net', password: 'admin@1234' };
    const remenberMeAction = USER.remember_me(data);
    const receivedData = {
      loggedIn: true,
      rememberMe: {
        email: 'jasdeep.singh@smartdatainc.net',
        password: 'admin@1234'
      }
    };
    // eslint-disable-next-line
    expect(user(intl.login, remenberMeAction)).toEqual(receivedData);
  });

  test('logout without remeber me', () => {
    const logoutAction = USER.log_out();
    expect(user(initialState, logoutAction)).toEqual(initialState);
  });

  test('logout with remeber me', () => {
    const logoutAction = USER.log_out(),
      expectedData = {
        ...initialState,
        rememberMe: {
          email: 'jasdeep.singh@smartdatainc.net',
          password: 'admin@1234'
        }
      };
    const receivedData = {
      loggedIn: false,
      rememberMe: {
        email: 'jasdeep.singh@smartdatainc.net',
        password: 'admin@1234'
      }
    };
    expect(user(expectedData, logoutAction)).toEqual(receivedData);
  });
});
