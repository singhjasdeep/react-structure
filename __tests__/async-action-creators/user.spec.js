//https://redux.js.org/recipes/writingtests
import moxios from 'moxios';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import * as USER from '../../src/actions/user';
import { AUTH_URL, CORE_URL } from '../../src/constants/connection';
import * as TYPE from '../../src/constants/action-types';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Async user actions', () => {
  beforeEach(() => moxios.install());
  afterEach(() => moxios.uninstall());

  it('creates when successfull auth0 login', () => {
    const store = mockStore({});
    const paramsData = {
      email: 'jasdeep.singh@smartdatainc.net',
      password: 'admin@1234'
    };
    const response = {
      access_token: 'C0MGZduFfuQil3wXyKsFeZIcAsnatsfU',
      expires_in: 86400,
      id_token:
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IlFqSkdRak5CUlRsRVEwRXhRelpHTnpWQk16RXhRVFE1UTBaR1JEazFNVGhDUWpRMk1VUkNNUSJ9.eyJuaWNrbmFtZSI6Imphc2RlZXBzaW5naDUxIiwibmFtZSI6Imphc2RlZXAuc2luZ2hAc21hcnRkYXRhaW5jLm5ldCIsInBpY3R1cmUiOiJodHRwczovL3MuZ3JhdmF0YXIuY29tL2F2YXRhci9hODI5YmNiODBkMmQzNjdiZTI5ODkyZjAyZjMyZjk5ZT9zPTQ4MCZyPXBnJmQ9aHR0cHMlM0ElMkYlMkZjZG4uYXV0aDAuY29tJTJGYXZhdGFycyUyRmphLnBuZyIsInVwZGF0ZWRfYXQiOiIyMDE4LTEwLTA0VDA0OjUxOjQ2LjY0MVoiLCJlbWFpbCI6Imphc2RlZXAuc2luZ2hAc21hcnRkYXRhaW5jLm5ldCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJpc3MiOiJodHRwczovL2NvcmUtZGVtby5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NWI3MTE3OTQ4NTdjYTIwZmU5NGQ3NTY2IiwiYXVkIjoiNmJvaTV2Q2lUaDVUZUsxMWIwUlFRY1FUbEQwMDdMenEiLCJpYXQiOjE1Mzg2Mjg3MDgsImV4cCI6MTUzODY2NDcwOH0.CvZVth9mlbfpHJiD0h9ofakuX9zEH2QrGasZE2RO2wTjuaQbW00MwZKDHqUPkNyFNLjTsqw8lspftnYS7YZtrHQxJjoSbnLud7CxFRJ3l4_EWJ3yD5IQW8jbY-EM9NwEezh06POQE-xw7ZggSCFjY2ou0z9Mvks5pVHCU6nVNDSoaPgMHIVAOZyE7TmJ6AoZZ5xWMftd8EwariHoogCpX-onLgodUAF6Stk3lHmU9vkpy11GLT5UVrsNp6UcXD9Bbx5dkOds2rChUP5tu3jfACtXBuexIaZyz23uwoEiwZHzfyB9L1PJPbFEGdU8qwlzf5QFkHD2y9ra4oWsnAisyQ',
      scope: 'openid profile email address phone',
      token_type: 'Bearer'
    };

    moxios.stubRequest(AUTH_URL, {
      status: 200,
      response: response
    });

    const expectedActions = [{ type: TYPE.REMEMBER_ME, data: { email: '', password: '' } }];

    store.dispatch(
      USER.login(paramsData, result => {
        expect(store.getActions()).toEqual(expectedActions);
        expect(result).toEqual({ ...response, status: true });
      })
    );
  });

  it('creates at logout user', () => {
    const store = mockStore({});
    const expectedActions = [{ type: TYPE.LOG_OUT }];
    store.dispatch(
      USER.logOut(result => {
        expect(store.getActions()).toEqual(expectedActions);
        expect(result).toEqual(true);
      })
    );
  });
});
