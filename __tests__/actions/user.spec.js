import * as USER from '../../src/actions/user';
import * as TYPE from '../../src/constants/action-types';

describe('User actions', () => {
  it('should create an action to login user', () => {
    const data = {
      nickname: 'jasdeepsingh51',
      name: 'jasdeep.singh@smartdatainc.net',
      picture:
        'https://s.gravatar.com/avatar/a829bcb80d2d367be29892f02f32f99e?s=480&r=pg&d=https%3A%2F%2Fcdn.auth0.com%2Favatars%2Fja.png',
      updated_at: '2018-08-28T05:11:01.078Z',
      email: 'jasdeep.singh@smartdatainc.net',
      email_verified: true,
      user_id: 12,
      tenant_id: 2
    };
    const expectedAction = {
      type: TYPE.LOGIN_SUCCESS,
      data
    };
    expect(USER.login_Success(data)).toEqual(expectedAction);
  });

  it('should create an action to remember me user', () => {
    const data = {
      email: 'jasdeep.singh@smartdatainc.net',
      password: 'admin@1234'
    };
    const expectedAction = {
      type: TYPE.REMEMBER_ME,
      data
    };
    expect(USER.remember_me(data)).toEqual(expectedAction);
  });

  it('should create an action to logout user', () => {
    const expectedAction = {
      type: TYPE.LOG_OUT
    };
    expect(USER.log_out()).toEqual(expectedAction);
  });
});
