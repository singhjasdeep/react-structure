import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { MemoryRouter } from 'react-router';
import { PersistGate } from 'redux-persist/lib/integration/react';
import Login from '../../src/containers/Login';
import NotFound from '../../src/components/NotFound';
import Routers from '../../src/config/Routers';

jest.mock('../../src/assets/images/logo.png', () => jest.fn());
jest.mock('../../src/assets/images/hamburgr.png', () => jest.fn());
jest.mock('../../src/assets/images/message_float.png', () => jest.fn());
jest.mock('react-datepicker/dist/react-datepicker-cssmodules.css', () => jest.fn());
jest.mock('react-toastify/dist/ReactToastify.css', () => jest.fn());

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Define Router test cases', () => {
  const store = mockStore({});

  test('with valid path should not redirect to 404', () => {
    // eslint-disable-next-line
    const wrapper = render(
      <Provider store={store}>
        <PersistGate>
          <MemoryRouter initialEntries={['/']}>
            <Routers {...store} />
          </MemoryRouter>
        </PersistGate>
      </Provider>
    );
    expect(wrapper.find(Login)).toMatchSnapshot();
  });

  test('with invalid path should redirect to 404', () => {
    // eslint-disable-next-line
    const wrapper = render(
      <Provider store={store}>
        <PersistGate>
          <MemoryRouter initialEntries={['/random']}>
            <Routers {...store} />
          </MemoryRouter>
        </PersistGate>
      </Provider>
    );
    expect(wrapper.find(NotFound)).toMatchSnapshot();
  });
});
