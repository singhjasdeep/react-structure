import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';
import * as AUTH from '../../src/auth';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const token =
  'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3QiLCJpYXQiOjE1MzgwMjU0NDksImV4cCI6MTg1MzQzMzQ0OSwibmJmIjoxNTM4MDI1NDQ5LCJqdGkiOiJ';
const store = mockStore({
  user: {
    loggedIn: true,
    nickname: 'test',
    email: 'test@gmail.com',
    email_verified: true,
    token
  }
});

describe('Auth functions', () => {
  it('creates when get User from store', () => {
    const user = AUTH.User(store);
    expect(user).toEqual(store.getState().user);
  });

  it('creates when routing authentication middleware', () => {
    const auth = AUTH.Auth(store);
    const loggedIn = store.getState().user.loggedIn;
    expect(auth).toEqual(loggedIn);
  });

  it('creates when set Authorization token in header', () => {
    const setAuthorizationToken = AUTH.setAuthorizationToken(axios, token);
    expect(setAuthorizationToken).toBe(undefined);
  });

  it('creates when set delete Authorization token in header', () => {
    const setAuthorizationToken = AUTH.setAuthorizationToken(axios);
    expect(setAuthorizationToken).toBe(undefined);
  });
});
